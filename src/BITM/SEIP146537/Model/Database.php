<?php

namespace App\Model;
use PDO;
use PDOException;

print_r(PDO::getAvailableDrivers());     // code for checking how many database are here in the system
echo"<br>";

class Database{

    public $DBH;
    public $host="localhost";
    public $dbname="atomic_project_b35";
    public $user="root";
    public $password="";

    public function __construct()                    // we should use construct method for database connection coding.
    {

        try {

            # MySQL with PDO_MYSQL
            $DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->password);

            echo "Connected Successfully!!!";
        }

        catch(PDOException $e){
            echo $e->getMessage();

        }
    }
}

//$objDatabase= new Database();