<?php

namespace App\BookTitle;

use App\Model\Database as DB;
//use PDO;
require_once("../../../../vendor/autoload.php");
class BookTitle extends DB{

    public $id;
    public $book_title;
    public $author_name;

    public function __construct()
    {
        parent::__construct();    // here extended database is the parent method. To call here the database that means to connect database we need to call parent in
    }
}
$objBookTitle= new BookTitle();
